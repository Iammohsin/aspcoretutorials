﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASPCoreTutorial1.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace ASPCoreTutorial1
{
    public class Startup
    {
        private IConfiguration _Config;

        //Part-9 : Here we will inject IConfiguration service : dependency injection 
        // to create constructor press ctor and Tab, below constructor will form
        //NOTE : IConfiguration service is setup to read configuration information from all the various configuration sources in asp.net core
        public Startup(IConfiguration config)
        {
            _Config = config;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
           services.AddMvc().AddXmlSerializerFormatters();
            // services.AddMvcCore();
            services.AddSingleton<IEmployeeRepository, MockEmployeeRepository>();
           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILogger<Startup> Logg)
        {

            if (env.IsDevelopment())
            {
                DeveloperExceptionPageOptions developerExceptionPageOptions = new DeveloperExceptionPageOptions
                {
                    SourceCodeLineCount = 1
                };
                app.UseDeveloperExceptionPage(developerExceptionPageOptions);
            }



            //Point-12 : Here we are accessing static files for request by using  app.UseStaticFiles(); 
            #region Point 12 Using Default Files and Static File MiddleWare 
            //DefaultFilesOptions DFO = new DefaultFilesOptions();
            //DFO.DefaultFileNames.Clear();
            //DFO.DefaultFileNames.Add("myhtml.html");
            //app.UseDefaultFiles(DFO);
            //app.UseStaticFiles();
            #endregion Point 12 Using Default Files and Static File MiddleWare 
            #region Point 12 By using server File Middleware which combines above two middleware 
            //FileServerOptions FSO = new FileServerOptions();
            //FSO.DefaultFilesOptions.DefaultFileNames.Clear();
            //FSO.DefaultFilesOptions.DefaultFileNames.Add("myhtml3.html");
            //app.UseFileServer(FSO);
            #endregion Point 12 By using server File Middleware which combines above two middleware

            // Point-13 : Exception Page 
            #region Point 13 Exception Page 
            //DeveloperExceptionPageOptions developerExceptionPageOptions = new DeveloperExceptionPageOptions
            //{
            //    SourceCodeLineCount = 1
            //};
            //app.UseDeveloperExceptionPage(developerExceptionPageOptions);
            #endregion Point 13 Exception Page 

            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
            //app.Run(async (context) =>
            //{
            //    //throw new Exception("Some error processing the request");
            //    await context.Response.WriteAsync("Hello World");
            //});

            #region Till Point 11
            //app.Use(async (context, Next) =>
            //{
            //    //await context.Response.WriteAsync("Hello world from 1st middleware");
            //    Logg.LogInformation("MD1 : First MD with request");
            //    await Next();
            //    Logg.LogInformation("MD1 : First MD with response");
            //});
            ////Point-11 : here we will see multiple middleware and how they passes request response to next middleware. 
            //// To call next middleware we use the methode Use instead Run
            //// Also then we create multiple middleware with logger and can see how request pipline proceessed 
            //app.Use(async (context, Next) =>
            //{
            //    //await context.Response.WriteAsync("Hello world from 2nd middleware");
            //    Logg.LogInformation("MD2 : Scond MD with request");
            //    await Next();
            //    Logg.LogInformation("MD2 : Scond MD with response");
            //});

            //app.Use(async (context, Next) =>
            //{
            //    //await context.Response.WriteAsync("Hello world from 2nd middleware");
            //    Logg.LogInformation("MD3 : Third MD with request");
            //    await context.Response.WriteAsync("We Are in third MiddleWare !");
            //    await Next();
            //    Logg.LogInformation("MD3 : Third MD with response");
            //});
            #endregion Till Point 11

           
        }
    }
}
