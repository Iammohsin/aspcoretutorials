﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCoreTutorial1.Models
{
    public enum Dept
    {
        None,
        HR,
        Payroll,
        IT
    }
}
