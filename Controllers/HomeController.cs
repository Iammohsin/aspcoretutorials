﻿using ASPCoreTutorial1.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ASPCoreTutorial1.ViewModels.ViewModels;

namespace ASPCoreTutorial1.Controllers
{
    public class HomeController : Controller
    {
        private IEmployeeRepository _employeeRepository;

        public HomeController(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        //[Route("")]
        public ViewResult Index()
        {
            HomeDetailsViewModel HDVM = new HomeDetailsViewModel();
            {
                HDVM.Employee = _employeeRepository.GetEmployee(1);
                HDVM.Title = "We Builds Your Dreams !!";
            }
            //return View("../CustomeView/index");
            //return View(HDVM);
            //return _employeeRepository.GetEmployee(1).Name;
            return View("Views/CustomeView/Index.cshtml", HDVM);
        }

        public ViewResult Details(int Id)
        {

            //Employee Model = _employeeRepository.GetEmployee(1);
            //ViewBag.Employee = Model;
            //ViewBag.Title = "Employee Details from Strongly Typed";
            HomeDetailsViewModel HDVM = new HomeDetailsViewModel();
            {
                HDVM.Employee = _employeeRepository.GetEmployee(Id);
                HDVM.Title = "We Builds Your Dreams !!";
            }
            //return View("../CustomeView/index");
            return View(HDVM);
        }

        //[Route("Home/AllDetails")]
        public ViewResult AllDetails()
        {
            // retrieve all the employees
            var model = _employeeRepository.GetAllEmployees();
            // Pass the list of employees to the view
            return View(model);
        }

        [HttpGet]
        public ViewResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Employee employee)
        {
            if (ModelState.IsValid)
            {
                Employee newEmp = _employeeRepository.AddNew(employee);

                return RedirectToAction("Details", new { Id = newEmp.Id });
            }
            else
            {
                return View();
            }
          
        }

       

       
    }
}
