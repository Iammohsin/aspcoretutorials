﻿using ASPCoreTutorial1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCoreTutorial1.ViewModels
{
    public class ViewModels
    {
        public class HomeDetailsViewModel
        {
            public Employee Employee { get; set; }
            public string Title { get; set; }
        }
           
    }
}
